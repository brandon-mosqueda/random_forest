# Una empresa dedicada a la venta de puercos necesita crear una máquina
# predictiva que le permita predecir el peso de sus puercos a los dos años de 
# edad, edad en que son puestos a la venta, para poder saber en cuáles se deben
# de enfocar durante el proceso de crecimiento.
# Las variables predictoras que se considerarán son: el peso actual del cerdo,
# el peso de la madre a sus dos años, el peso del padre a sus dos años, el tipo
# de alimentación que llevan (dentro de 3 diferentes tipos) y el sexo.
rm(list=ls())
source('utils.R')

PIGS_NUM <- 20
COURSES_NUM <- 4
SEED <- 123

col_names <- c("Peso_actual", "Peso_madre", "Peso_padre")

pigs_weights <- generate_random_continuos_matrix(PIGS_NUM,
                                                 c(11, 124, 170),
                                                 seed=SEED,
                                                 columns_names=col_names)

feedings <- as.factor(sample(c("Alimentacion_1", "Alimentacion_2", "Alimentacion_3"), 
                             PIGS_NUM, replace=TRUE, prob=c(1/3, 1/3, 1/3)))
feedings_numeric <- as.numeric(feedings)

sexs <- as.factor(c(rep("Male", PIGS_NUM / 2), rep("Female", PIGS_NUM / 2)))
sexs_numeric <- as.numeric(sexs)

X <- data.frame(pigs_weights, Alimentacion=feedings, Sexo=sexs)

normalized_pigs_weights <- normalize(as.matrix(X[, 1:3])) + 3
betas <- c(5.79, 7.72, 6.63)
Y <- normalized_pigs_weights %*% betas
Y
Y[feedings_numeric == 1] <- Y[feedings_numeric == 1] + 20.96
Y[feedings_numeric == 2] <- Y[feedings_numeric == 2] + 26.32
Y[feedings_numeric == 3] <- Y[feedings_numeric == 3] + 21.47
Y

# Macho
Y[sexs_numeric == 2] <- Y[sexs_numeric == 2] + 68
# Hembra
Y[sexs_numeric == 1] <- Y[sexs_numeric == 1] + 18
Y
Y <- Y + rnorm(PIGS_NUM, 28, 4)
Y

# Hembras 100 - 150
# Machos 150 - 200
X[, 1:3] <- round(X[, 1:3], 2)
Y <- round(Y, 2)

save(X, Y, file="continuous_examples/3_pigs_weights/dataset.RData")