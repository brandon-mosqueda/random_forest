# Usando el muy conocido conjunto de datos con información de las plantas iris
# se pretende generar un modelo que sea capaz de predecir la longitud del 
# sépalo de una de estas plantas tomando como variables predictoras la longitd
# del pétalo, la anchura del pétalo y el tipo de planta al que pertenece (de los
# 3 diferentes tipos que existen).
rm(list=ls())
library(randomForest)

# Establecer la semilla para poder reproducir los resultados
set.seed(408)

# Importar el conjunto de datos
# Variables predictoras: X  [20, 3]
# Variable respuesta: Y  [20]
load("continuous_examples/2_iris_sepal_length/dataset.RData")

# La proporción de observaciones que serán utilizadas para el entrenamiento
training_proportion <- 0.7

sample_length <- dim(X)[[1]]
predictors_num <- dim(X)[[2]]
dataset <- data.frame(y=Y, X)
head(dataset)
# Generar los índices de los elementos que formarán parte del entrenamiento
training_indexes <- sample(sample_length,
                           training_proportion * sample_length,
                           replace=FALSE)
training_set <- dataset[training_indexes, ]
testing_set <- dataset[-training_indexes, ]

# Mean Square Error of prediction function
mse <- function(observed, predicted) sum((observed - predicted)^2) / length(observed)

# Create a random forest model
model <- randomForest(y ~ ., data=training_set, ntree=500,
                      mtry=predictors_num / 3, importance=TRUE)
model

# Predicting on training set
predictions_training <- predict(model, training_set)

# Checking classification accuracy
predictions_training 
MSE_training <- mse(training_set$y, predictions_training)
MSE_training

# Predicting on Validation set
X_testing_set <- testing_set[, -1] # Quitar las Y
predictions_validation <- predict(model, X_testing_set)

# Checking classification accuracy
predictions_validation
MSE_testing <- mse(testing_set$y, predictions_validation)
MSE_testing

# To check important variables
importance(model)
varImpPlot(model)

# Using for loop to identify the right mtry for model (hyperparameter tuning).
# mtry: Number of variables randomly sampled as candidates at each split. Note
# that the default values are different for classification (sqrt(p) where p is
# number # of variables in x) and regression (p/3)
MSE <- c()

for (samples_num_per_split in 1:predictors_num) {
  print(samples_num_per_split)
  tuned_model <- randomForest(y ~ ., data=training_set,
                              ntree=500, mtry=samples_num_per_split,
                              importance=TRUE)

  predictions_validation <- predict(tuned_model, testing_set)
  MSE <- c(MSE, mse(testing_set$y, predictions_validation))
}
MSE

plot(1:predictors_num, MSE)