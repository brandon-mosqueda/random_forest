library(datasets)

set.seed(408)

# Índices de las columnas del dataset original que guardan información del 
# sépalo de las flores
sepal_info_indexes <- 1:2
# ïndice de la columna donde se guarda la información de la longitud del sépalo
sepal_length_index <- 1
# Se tomarán 20 muestras, (7, 7 y 6) de las tres categorías
samples_indexes <- c(1:7, 51:57, 101:106)

# Generar el dataset de variables predictoras eliminado la información de los 
# sépalos
X <- iris[samples_indexes, -sepal_info_indexes]

# Guardar la información de la longitud de los sépalos como la variable
# respuesta
Y <- iris[samples_indexes, sepal_length_index]

save(X, Y, file="continuous_examples/2_iris_sepal_length/dataset.RData")