# Una Universidad no podrá realizar los exámenes de admisión a una de sus
# maestrías, por lo que se ha sugerido generar un modelo de tipo Random Forest,
# que, usando como muestras de entrenamiento la información de los estudiantes
# que aplicaron el año pasado, pueda estimar la calificación en los exámenes de 
# los que van a aplicar y no van a poder presentarlo.
# La idea es generar un modelo que tome de entrada la calificación de los
# estudiantes en 4 de las materias relacionadas al área y que estime la
# calificación que se obtendrá en el examen.
rm(list=ls())
library(randomForest)

# Establecer la ruta del directorio en donde se tienen los archivos porque si no
# habrá un error al importar el conjunto de datos.
# setwd("my_work_directory")

# Importar el conjunto de datos
# Variables predictoras: "courses_grades" [20, 4]
# Variable respuesta: "exams_grades" [20]
load("continuous_examples/1_exam_grades/dataset.RData")

# Establecer la semilla para poder reproducir los resultados
set.seed(334)

# La proporción de observaciones que serán utilizadas para el entrenamiento
training_proportion <- 0.75

sample_length <- dim(courses_grades)[[1]]
predictors_num <- dim(courses_grades)[[2]]

dataset <- data.frame(y=exams_grades, courses_grades)
head(dataset)
# Generar los índices de los elementos que formarán parte del entrenamiento
training_indexes <- sample(sample_length,
                           training_proportion * sample_length,
                           replace=FALSE)
training_set <- dataset[training_indexes, ]
testing_set <- dataset[-training_indexes, ]

# Mean Square Error of prediction function
mse <- function(observed, predicted) sum((observed - predicted)^2) / length(observed)

# Create a random forest model
model <- randomForest(y ~ ., data=training_set, ntree=500,
                      mtry=predictors_num / 3, importance=TRUE)
model

# Predicting on training set
predictions_training <- predict(model, training_set)

# Checking classification accuracy
predictions_training
MSE_training <- mse(training_set$y, predictions_training)
MSE_training

# Predicting on validation set
Xs_testing_set <- testing_set[, -1] # Quitar la variable respuesta
predictions_validation <- predict(model, Xs_testing_set)

# Checking classification accuracy
predictions_validation
MSE_testing <- mse(testing_set$y, predictions_validation)
MSE_testing

# To check important variables
importance(model)
varImpPlot(model)

# Using for loop to identify the right mtry for model (hyperparameter tuning).
# mtry: Number of variables randomly sampled as candidates at each split. Note
# that the default values are different for classification (sqrt(p) where p is
# number # of variables in x) and regression (p/3)
MSE <- c()

for (samples_num_per_split in 1:predictors_num) {
  print(samples_num_per_split)
  tuned_model <- randomForest(y ~ ., data=training_set,
                              ntree=500, mtry=samples_num_per_split,
                              importance=TRUE)

  predictions_validation <- predict(tuned_model, testing_set)
  MSE <- c(MSE, mse(testing_set$y, predictions_validation))
}
MSE

plot(MSE)