rm(list=ls())
source('utils.R')

STUDENTS_NUM <- 20
COURSES_NUM <- 4
SEED <- 123

students <- paste0("Estudiante_", 1:STUDENTS_NUM)
courses <- paste0("Materia_", 1:COURSES_NUM)

courses_grades <- generate_random_continuos_matrix(STUDENTS_NUM,
                                                   c(7.65, 8.35, 9.11, 8.73),
                                                   seed=SEED,
                                                   max_value=10,
                                                   rows_names=students,
                                                   columns_names=courses)

courses_grades <- as.matrix(courses_grades)
normalized_grades <- scale(courses_grades) + 3
betas <- c(0.79, 0.72, 0.63, 0.68)
exams_grades <- normalized_grades %*% betas + rnorm(20, 0.4, 0.2)
exams_grades[exams_grades > 10] <- 10

courses_grades <- round(courses_grades, 2)
exams_grades <- round(exams_grades, 2)

save(courses_grades,
     exams_grades,
     file="continuous_examples/1_exam_grades/dataset.RData")