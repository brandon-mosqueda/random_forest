RSS = sum from {i=1} to {L} {(y_i - overline {y_L})^2} +
      sum from {i=1} to {R} {(y_i - overline {y_R})^2} +

matrix{
  alignr RSS # "=" #
    alignl (7.11 - 7.45)^2 + (7.84 - 7.45)^2 +
           (9.98 - 9.5514)^2 + dotslow + (10.00 - 9.5514)^2 + (10.00 - 9.5514)^2 ##
  alignr "" # "=" #
    alignl 0.2664 + 2.2584 ##
  alignr "" # "=" #
    alignl 2.5248
}

matrix {
  alignr MSE # "=" #
    alignl  {(8.7 - 10)^2 + (9.98 - 9.07)^2 + (8.7 - 6.41)^2} over {3} ##
  alignr "" # "=" #
    alignl {1.69 + 0.8281 + 5.244} over {3} ##
  alignr "" # "=" #
    alignl {7.7621} over {3} ##
  alignr "" # "=" #
    alignl 2.5874
}