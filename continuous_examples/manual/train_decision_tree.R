rm(list=ls())

# Set a seed for reproducible results
set.seed(932)

source("manual_operations.R")

# Import dataset
load("Grades.RData")
Grades

dim(Grades)

N <- nrow(Grades)
testing_indices <- c(12, 5, 3)
testing_indices
training_indices <- (1:N)[-testing_indices]
exam_grade_column <- 1

Training_set <- Grades[training_indices, ]
Testing_set <- Grades[-training_indices, ]

tree <- decision_tree(Training_set[, -exam_grade_column],
                      Training_set$exam_grade, 10)
predictions <- predict_dt(tree,
                          Testing_set[, -exam_grade_column])
data.frame(predictions, true_values=Testing_set$exam_grade)

mse(Testing_set$exam_grade, predictions)

print_tree(tree)