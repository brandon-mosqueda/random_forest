rm(list=ls())
library(dplyr)
set.seed(123)

# fifa <- read.csv("/home/rstudio/Datasets/players_20.csv")
fifa <- read.csv("C:\\Users\\Brandon Mosqueda\\Documents\\Profesional_practices\\Datasets/players_20.csv")
necaxa <- fifa[fifa$club == "Club Necaxa", ]
euro_to_peso <- 20.62

players <- select(necaxa, age, potential, movement_agility, power_stamina)
players$positions <- as.factor(as.character(necaxa$team_position))
rownames(players) <- necaxa$short_name

players_sample <- sample(dim(players)[[1]], 20)

players <- players[players_sample, ]
wages <- necaxa[players_sample, ]$wage_eur
wages <- wages * euro_to_peso

save(players, wages, file="continuous_examples/4_necaxa_wages/dataset.RData")