# Remove all variables from our workspace
# rm(list=ls(all=TRUE))

# Set working directory
setwd("~/Data_Science/random_forest/rfsrc_examples")

# Install the needed version of randomForestSRC library from this Github repo
# devtools::install_github("brandon-mosqueda/randomForestSRC")

library(randomForestSRC)
library(dplyr)
library(caret)
library(purrr)

# Import some useful functions as CV.Random, CV.Kfold, mse, maape, etc.
source("utils.R")

# Import the dataset
load("Data_Toy_EYT.RData", verbose=TRUE)
Pheno <- Pheno_Toy_EYT
Geno <- G_Toy_EYT

########## PREPARE DATA ##########
Pheno$Env <- as.factor(Pheno$Env)
Pheno$Height <- as.factor(Pheno$Height)

# Sorting data
Pheno <- Pheno[order(Pheno$Env, Pheno$GID), ]
geno_sort_lines <- sort(rownames(Geno))
Geno <- Geno[geno_sort_lines, geno_sort_lines]

### Design matrices definition ###
ZG <- model.matrix(~0 + GID, data=Pheno)
# Compute the Choleski factorization
ZL <- chol(Geno)
ZGL <- ZG %*% ZL

ZE <- model.matrix(~0 + Env, data=Pheno)
# Interaction design matrix
ZGE <- model.matrix(~0 + ZGL:Env, data=Pheno)

# Bind all design matrices in a single matrix to be used as predictor
X <- cbind(ZGL, ZE, ZGE)
dim(X)

# Create a data frame with the information of response variable and all
# predictors. As Heigth is factor automatically it will be trained a classifier
# random forest.
Data <- data.frame(y=Pheno$Height, X)
head(Data[, 1:5])

n_records <- nrow(Pheno)
n_outer_folds <- 5
n_inner_folds <- 5

# Get the indices of the elements that are going to be used as training and
# testing in each fold
outer_folds <- CV.Kfold(n_records, k=n_outer_folds)

# Define the values which are going to be evaluated in the tuning process
tuning_values <- list(ntrees=c(100, 200, 300),
                      mtry=c(80, 100, 120),
                      nodesize=c(3, 6, 9))

# Get all possible combinations of the defined tuning values -(3 * 3 * 3)
all_combinations <- cross(tuning_values)
n_combinations <- length(all_combinations)

########## RANDOM FOREST TUNING AND EVALUATION ##########
# Define the variable where the final results of each fold will be stored in
Predictions <- data.frame()

# Iterate over each generated fold
for (i in 1:n_outer_folds) {
  cat("Outer Fold:", i, "/", n_outer_folds, "\n")
  outer_fold <- outer_folds[[i]]

  # Divide our data in testing and training sets
  DataTraining <- Data[outer_fold$training, ]
  DataTesting <- Data[outer_fold$testing, ]

  ### Tuning only with training data ###
  n_tuning_records <- nrow(DataTraining)
  # Variable that will hold the best combination of hyperparameters and the PCCC
  # that produced.
  best_params <- list(pccc=-Inf)

  inner_folds <- CV.Kfold(n_tuning_records, k=n_inner_folds)

  for (j in 1:n_combinations) {
    cat("\tCombination:", j, "/", n_combinations, "\n")

    flags <- all_combinations[[j]]

    cat("\t\tInner folds: ")
    for (m in 1:n_inner_folds) {
      cat(m, ", ")
      inner_fold <- inner_folds[[m]]

      DataInnerTraining <- DataTraining[inner_fold$training, ]
      DataInnerTesting <- DataTraining[inner_fold$testing, ]

      # Fit the model using the current combination of hyperparameters
      tuning_model <- rfsrc(y ~ ., data=DataInnerTraining, ntree=flags$ntree,
                            mtry=flags$mtry, nodesize=flags$nodesize)
      predictions <- predict(tuning_model, newdata=DataInnerTesting)$class

      # Compute PCCC for the current combination of hyperparameters
      current_pccc <- pccc(DataInnerTesting$y, predictions)

      # If the current combination gives a greater PCCC set it as new best_params
      if (current_pccc > best_params$pccc) {
        best_params <- flags
        best_params$pccc <- current_pccc
      }
    }
    cat("\n")
  }

  # Using the best hyper-params combination retrain the model but using the complete
  # training set
  model <- rfsrc(y ~ ., data=DataTraining, ntree=best_params$ntree,
                 mtry=best_params$mtry, nodesize=best_params$nodesize)
  predicted <- predict(model, newdata=DataTesting)$class

  # Save the information of the predictions in the current fold
  CurrentPredictions <- data.frame(Position=outer_fold$testing,
                                   GID=Pheno$GID[outer_fold$testing],
                                   Env=Pheno$Env[outer_fold$testing],
                                   Partition=i,
                                   Observed=DataTesting$y,
                                   Predicted=predicted)
  Predictions <- rbind(Predictions, CurrentPredictions)
}

head(Predictions)
tail(Predictions)

# Summarise the results across environment computing 2 metrics
ByEnvSummary <- Predictions %>%
                # Calculate the metrics disaggregated by Partition and Env
                group_by(Partition, Env) %>%
                summarise(PCCC=pccc(Observed, Predicted),
                          Kappa=kappa(Observed, Predicted)) %>%
                select_all() %>%

                # Calculate the metrics disaggregated Env with standard errors
                # of each partitions
                group_by(Env) %>%
                summarise(SE_PCCC=sd(PCCC, na.rm=TRUE) / sqrt(n()),
                          PCCC=mean(PCCC, na.rm=TRUE),
                          SE_Kappa=sd(Kappa, na.rm=TRUE) / sqrt(n()),
                          Kappa=mean(Kappa, na.rm=TRUE)) %>%
                select_all() %>%

                mutate_if(is.numeric, ~round(., 4)) %>%
                as.data.frame()
ByEnvSummary

write.csv(Predictions, file="results/3.Height_k_fold_all.csv", row.names=FALSE)
write.csv(ByEnvSummary, file="results/3.Height_k_fold_summary.csv", row.names=FALSE)