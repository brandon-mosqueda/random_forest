# Remove all variables from our workspace
# rm(list=ls(all=TRUE))

# Set working directory
setwd("~/Data_Science/random_forest/rfsrc_examples")

# Install the needed version of randomForestSRC library from this Github repo
# that containts the zap.rfsrc function if not installed or if you have another
# version of randomForestSRC
# devtools::install_github("brandon-mosqueda/randomForestSRC")

library(randomForestSRC)
library(dplyr)
library(caret)
library(purrr)

# Import some useful functions as CV.Random, CV.Kfold, mse, maape, etc.
source("utils.R")

# Import the dataset
load("Data_Toy_EYT.RData", verbose=TRUE)
Pheno <- Pheno_Toy_EYT
Pheno$Env <- as.factor(Pheno$Env)
Geno <- G_Toy_EYT

# Sorting data
Pheno <- Pheno[order(Pheno$Env, Pheno$GID), ]
geno_sort_lines <- sort(rownames(Geno))
Geno <- Geno[geno_sort_lines, geno_sort_lines]

### Design matrices definition ###
ZG <- model.matrix(~0 + GID, data=Pheno)
# Compute the Choleski factorization
ZL <- chol(Geno)
ZGL <- ZG %*% ZL

ZE <- model.matrix(~0 + Env, data=Pheno)
# Interaction design matrix
ZGE <- model.matrix(~0 + ZGL:Env, data=Pheno)

# Bind all design matrices in a single matrix to be used as predictor
X <- data.frame(cbind(ZGL, ZE, ZGE))
dim(X)

# Response variable
y <- Pheno$DTHD

n_records <- nrow(Pheno)
n_outer_folds <- 5
n_inner_folds <- 5

# Get the indices of the elements that are going to be used as training and
# testing in each fold
outer_folds <- CV.Kfold(n_records, k=n_outer_folds)

# Define the values which are going to be evaluated in the tuning process
tuning_values <- list(ntrees=c(100, 200, 300),
                      mtry=c(80, 100, 120),
                      nodesize=c(3, 6, 9))

# Get all possible combinations of the defined tuning values -(3 * 3 * 3)
all_combinations <- cross(tuning_values)
n_combinations <- length(all_combinations)

########## RANDOM FOREST TUNING AND EVALUATION ##########
# Define the variable where the final results of each fold will be stored in
Predictions <- data.frame()

# Iterate over each generated fold
for (i in 1:n_outer_folds) {
  cat("Outer Fold:", i, "/", n_outer_folds, "\n")
  outer_fold <- outer_folds[[i]]

  # Divide our data in testing and training sets
  X_training <- X[outer_fold$training, ]
  y_training <- y[outer_fold$training]

  X_testing <- X[outer_fold$testing, ]
  y_testing <- y[outer_fold$testing]

  ### Tuning only with training data ###
  n_tuning_records <- nrow(X_training)
  # Variable that will hold the best combination of hyperparameters and the MSE
  # that produced.
  best_params <- list(mse=Inf)

  inner_folds <- CV.Kfold(n_tuning_records, k=n_inner_folds)

  for (j in 1:n_combinations) {
    cat("\tCombination:", j, "/", n_combinations, "\n")

    flags <- all_combinations[[j]]

    cat("\t\tInner folds: ")
    for (m in 1:n_inner_folds) {
      cat(m, ", ")
      inner_fold <- inner_folds[[m]]

      X_inner_training <- X_training[inner_fold$training, ]
      y_inner_training <- y_training[inner_fold$training]

      X_inner_testing <- X_training[inner_fold$testing, ]
      y_inner_testing <- y_training[inner_fold$testing]

      # Fit the model using the current combination of hyperparameters
      tuning_model <- zap.rfsrc(X_inner_training, y_inner_training,
                                ntree_theta=flags$ntree,
                                mtry_theta=flags$mtry,
                                nodesize_theta=flags$nodesize,
                                ntree_lambda=flags$ntree,
                                mtry_lambda=flags$mtry,
                                nodesize_lambda=flags$nodesize)
      # You can also use custom as prediction type
      predictions <- predict(tuning_model, X_inner_testing)$predicted

      # Compute MSE for the current combination of hyperparameters
      current_mse <- mse(y_inner_testing, predictions)

      # If the current combination gives a lower MSE set it as new best_params
      if (current_mse < best_params$mse) {
        best_params <- flags
        best_params$mse <- current_mse
      }
    }
    cat("\n")
  }

  # Using the best hyper-params combination retrain the model but using the complete
  # training set
  model <- zap.rfsrc(X_training, y_training,
                     ntree_theta=best_params$ntree,
                     mtry_theta=best_params$mtry,
                     nodesize_theta=best_params$nodesize,
                     ntree_lambda=best_params$ntree,
                     mtry_lambda=best_params$mtry,
                     nodesize_lambda=best_params$nodesize)
  # You can also use custom as prediction type
  predicted <- predict(model, X_testing, type="original")$predicted

  # Save the information of the predictions in the current fold
  CurrentPredictions <- data.frame(Position=outer_fold$testing,
                                   GID=Pheno$GID[outer_fold$testing],
                                   Env=Pheno$Env[outer_fold$testing],
                                   Partition=i,
                                   Observed=y_testing,
                                   Predicted=predicted)
  Predictions <- rbind(Predictions, CurrentPredictions)
}

head(Predictions)
tail(Predictions)

# Summarise the results across environment computing 4 metrics
ByEnvSummary <- Predictions %>%
                # Calculate the metrics disaggregated by Partition and Env
                group_by(Partition, Env) %>%
                summarise(MSE=mse(Observed, Predicted),
                          Cor=cor(Predicted, Observed, use="na.or.complete"),
                          R2=cor(Predicted, Observed, use="na.or.complete")^2,
                          MAAPE=maape(Observed, Predicted)) %>%
                select_all() %>%

                # Calculate the metrics disaggregated Env with standard errors
                # of each partitions
                group_by(Env) %>%
                summarise(SE_MAAPE=sd(MAAPE, na.rm=TRUE) / sqrt(n()),
                          MAAPE=mean(MAAPE, na.rm=TRUE),
                          SE_Cor=sd(Cor, na.rm=TRUE) / sqrt(n()),
                          Cor=mean(Cor, na.rm=TRUE),
                          SE_R2=sd(R2, na.rm=TRUE) / sqrt(n()),
                          R2=mean(R2, na.rm=TRUE),
                          SE_MSE=sd(MSE, na.rm=TRUE) / sqrt(n()),
                          MSE=mean(MSE, na.rm=TRUE)) %>%
                select_all() %>%

                mutate_if(is.numeric, ~round(., 4)) %>%
                as.data.frame()
ByEnvSummary

write.csv(Predictions, file="results/7.DTHD_k_fold_all.csv", row.names=FALSE)
write.csv(ByEnvSummary, file="results/7.DTHD_k_fold_summary.csv", row.names=FALSE)