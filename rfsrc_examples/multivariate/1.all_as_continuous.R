################################################################################
# Multivariate Random forest example with 4 continuous response variables
################################################################################

# Remove all variables from our workspace
# rm(list=ls(all=TRUE))

# Set working directory
setwd("~/Data_Science/random_forest/rfsrc_examples")

# Install the needed version of randomForestSRC library from this Github repo
# devtools::install_github("brandon-mosqueda/randomForestSRC")

library(randomForestSRC)
library(dplyr)
library(caret)
library(purrr)

# Import some useful functions as CV.Random, CV.Kfold, mse, maape, etc.
source("utils.R")

# Import the dataset
load("Data_Toy_EYT.RData", verbose=TRUE)
Pheno <- Pheno_Toy_EYT
Pheno$Env <- as.factor(Pheno$Env)

# Verify all variables are numeric
str(Pheno)

Geno <- G_Toy_EYT

# Sorting data
Pheno <- Pheno[order(Pheno$Env, Pheno$GID), ]
geno_sorted_lines <- sort(rownames(Geno))
Geno <- Geno[geno_sorted_lines, geno_sorted_lines]

### Design matrices definition ###
ZG <- model.matrix(~0 + GID, data=Pheno)
# Compute the Choleski factorization
ZL <- chol(Geno)
ZGL <- ZG %*% ZL

ZE <- model.matrix(~0 + Env, data=Pheno)
# Interaction design matrix
ZGE <- model.matrix(~0 + ZGL:Env, data=Pheno)

# Bind all design matrices in a single matrix to be used as predictor
X <- cbind(ZGL, ZE, ZGE)
dim(X)

# Create a data frame with the information of the 4 response variables and all
# predictors
Data <- data.frame(GY=Pheno$GY, DTHD=Pheno$DTHD,
                   DTMT=Pheno$DTMT, Height=Pheno$Height,
                   X)
head(Data[, 1:8])

responses <- c("GY", "DTHD", "DTMT", "Height")
n_records <- nrow(Pheno)
n_outer_folds <- 5
n_inner_folds <- 5

# Get the indices of the elements that are going to be used as training and
# testing in each fold
outer_folds <- CV.Kfold(n_records, k=n_outer_folds)

# Define the values which are going to be evaluated in the tuning process
tuning_values <- list(ntrees=c(100, 200, 300),
                      mtry=c(80, 100, 120),
                      nodesize=c(3, 6, 9))

# Get all possible combinations of the defined tuning values (3 * 3 * 3)
all_combinations <- cross(tuning_values)
n_combinations <- length(all_combinations)

########## RANDOM FOREST TUNING AND EVALUATION ##########
# Define the variable where the final results of each fold will be stored in
Predictions <- data.frame()

# Iterate over each generated fold
for (i in 1:n_outer_folds) {
  cat("Outer Fold:", i, "/", n_outer_folds, "\n")
  outer_fold <- outer_folds[[i]]

  # Divide our data in testing and training sets
  DataTraining <- Data[outer_fold$training, ]
  DataTesting <- Data[outer_fold$testing, ]

  ### Tuning only with training data ###
  n_tuning_records <- nrow(DataTraining)
  # Variable that will hold the best combination of hyperparameters and the
  # MAAPE that produced.
  best_params <- list(maape=Inf)

  inner_folds <- CV.Kfold(n_tuning_records, k=n_inner_folds)

  for (j in 1:n_combinations) {
    cat("\tCombination:", j, "/", n_combinations, "\n")

    flags <- all_combinations[[j]]

    cat("\t\tInner folds: ")
    for (m in 1:n_inner_folds) {
      cat(m, ", ")
      inner_fold <- inner_folds[[m]]

      DataInnerTraining <- DataTraining[inner_fold$training, ]
      DataInnerTesting <- DataTraining[inner_fold$testing, ]

      # Fit the multivariate model using the current combination of
      # hyperparameters
      tuning_model <- rfsrc(Multivar(GY, DTHD, DTMT, Height) ~ .,
                            data=DataInnerTraining, ntree=flags$ntree,
                            mtry=flags$mtry, nodesize=flags$nodesize)
      predictions <- predict(tuning_model, DataInnerTesting)

      # Compute MAAPE for all response variables with the current combination of
      # hyperparameters
      gy_maape <- maape(DataInnerTesting$GY,
                        predictions$regrOutput$GY$predicted)
      dthd_maape <- maape(DataInnerTesting$DTHD,
                          predictions$regrOutput$DTHD$predicted)
      dtmt_maape <- maape(DataInnerTesting$DTMT,
                          predictions$regrOutput$DTMT$predicted)
      height_maape <- maape(DataInnerTesting$Height,
                            predictions$regrOutput$Height$predicted)

      current_maape <- mean(c(gy_maape, dthd_maape, dtmt_maape, height_maape))

      # If the current combination gives a lower MAAPE set it as new best_params
      if (current_maape < best_params$maape) {
        best_params <- flags
        best_params$maape <- current_maape
      }
    }
    cat("\n")
  }

  # Using the best hyper-params combination retrain the model but using the complete
  # training set
  model <- rfsrc(Multivar(GY, DTHD, DTMT, Height) ~ .,
                 data=DataTraining, ntree=best_params$ntree,
                 mtry=best_params$mtry, nodesize=best_params$nodesize)
  predicted <- predict(model, DataTesting)

  CurrentPredictions <- data.frame()
  # Bind the predictions of each response variable in the current fold
  for (response_name in responses) {
    CurrentPredictions <- rbind(
      CurrentPredictions,
      data.frame(
        Position=outer_fold$testing,
        GID=Pheno$GID[outer_fold$testing],
        Env=Pheno$Env[outer_fold$testing],
        Partition=i,
        Trait=response_name,
        Observed=DataTesting[[response_name]],
        Predicted=predicted$regrOutput[[response_name]]$predicted
      )
    )
  }

  Predictions <- rbind(Predictions, CurrentPredictions)
}

head(Predictions)
tail(Predictions)

# Summarise the results across environment computing 4 metrics per response
ByEnvSummary <- Predictions %>%
                # Calculate the metrics disaggregated by Partition and Env
                group_by(Trait, Partition, Env) %>%
                summarise(MSE=mse(Observed, Predicted),
                          Cor=cor(Predicted, Observed, use="na.or.complete"),
                          R2=cor(Predicted, Observed, use="na.or.complete")^2,
                          MAAPE=maape(Observed, Predicted)) %>%
                select_all() %>%

                # Calculate the metrics disaggregated Env with standard errors
                # of each partitions
                group_by(Env, Trait) %>%
                summarise(SE_MAAPE=sd(MAAPE, na.rm=TRUE) / sqrt(n()),
                          MAAPE=mean(MAAPE, na.rm=TRUE),
                          SE_Cor=sd(Cor, na.rm=TRUE) / sqrt(n()),
                          Cor=mean(Cor, na.rm=TRUE),
                          SE_R2=sd(R2, na.rm=TRUE) / sqrt(n()),
                          R2=mean(R2, na.rm=TRUE),
                          SE_MSE=sd(MSE, na.rm=TRUE) / sqrt(n()),
                          MSE=mean(MSE, na.rm=TRUE)) %>%
                select_all() %>%

                # Order by Trait
                arrange(Trait) %>%

                mutate_if(is.numeric, ~round(., 4)) %>%
                as.data.frame()
ByEnvSummary

write.csv(Predictions,
          file="multivariate/results/1.all_as_continuous_all.csv",
          row.names=FALSE)
write.csv(ByEnvSummary,
          file="multivariate/results/1.all_as_continuous_summary.csv",
          row.names=FALSE)