# Remove all objects in your R's enviroment
rm(list=ls())

# Import the code with manual operations for training a decision tree.
source("manual_operations.R")

# Load the dataset
load("Iris.RData")

Iris

N <- nrow(Iris)
testing_indices <- c(4, 8, 12)
training_indices <- (1:N)[-testing_indices]
# Define a variable with response variable's column
specie_column <- 5

X_training <- Iris[training_indices, -specie_column]
y_training <- Iris[training_indices, specie_column]

X_testing <- Iris[testing_indices, -specie_column]
y_testing <- Iris[testing_indices, specie_column]
X_testing
y_testing

initial_impurity <- gini_impurity(y_training)
initial_impurity$impurity

thresholds <- data.frame(variable=rep(c("Sepal.Length",
										"Sepal.Width",
										"Petal.Length",
										"Petal.Width"), each=2),
					 	 threshold=c(6.1, 6.6,
						 			 3, 3.3,
						 			 1.2, 5.6,
						 			 0.2, 2.4),
					 	 stringsAsFactors=FALSE)

# Keep the threshold that gives the greatest Gini Gain
root_node <- get_best_gini_gain(X_training,
								y_training,
					  			initial_impurity$impurity,
					  			thresholds)
root_node

root_partition <- split_data(X_training, y_training,
							 root_node$variable,
							 root_node$threshold)
root_partition$left_branch
root_partition$right_branch

left_branch <- evaluate_node(root_partition$left_branch$X,
							 root_partition$left_branch$y,
							 5)

right_branch <- evaluate_node(root_partition$right_branch$X,
							  root_partition$right_branch$y,
							  5)

root_node$left_branch <- left_branch
root_node$right_branch <- right_branch

print_tree(root_node)

predictions <- predict_dt(root_node, X_testing)

data.frame(predictions, y_testing)

pccc(predictions, y_testing)