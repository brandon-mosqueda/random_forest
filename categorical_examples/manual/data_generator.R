rm(list=ls())

Iris <- iris[1:4, ]
Iris <- rbind(Iris, iris[51:54, ])
Iris <- rbind(Iris, iris[101:104, ])

rownames(Iris) <- 1:12
save(Iris, file="Iris.RData")