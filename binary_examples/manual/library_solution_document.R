rm(list=ls())

library(randomForest)

load("dataset.RData")
source("manual_operations.R")
source("plot_tree.R")

set.seed(658)

training_indexes <- 1:15

X_training <- X[training_indexes, ]
Y_training <- Y[training_indexes]

training_set <- data.frame(y=Y_training, X_training)
model <- randomForest(y ~ ., data=training_set, ntree=1,
			    	  mtry=3, importance=TRUE)
tree <- getTree(model, labelVar=TRUE)
plot_tree(tree)